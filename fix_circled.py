#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# I use a special \circle commands in LaTeX that draws a circle around a 
# number. So that to can even work in code sections, we need apply this fix after mdBook
# created the html output.

import os
import sys
import re
import toml

re_circle_fix = [r'circled{([0-9]+?)}', r'<text class="circled-code">\1</text>']

def fix_circled_in_code_sections(file):
    with open(file, 'r+') as f:
        content = f.read()
        content = re.sub(re_circle_fix[0], re_circle_fix[1], content)
        f.seek(0)
        f.truncate()
        f.write(content)
    

###############################################################################
if __name__ == "__main__":
    # read config
    if len(sys.argv) != 2:
        print('You must provide a config.toml file!')
        sys.exit(-1)
    config_file = sys.argv[1]
    config = toml.load(config_file)
    
    html_dir = config.get('markdown').get('mdbook').get('html').get('directory')
    suffix_chapters = config.get('markdown').get('mdbook').get('html').get('suffix')
    
    # traverse output directory, and run through all html files
    for root, dirs, files in os.walk(html_dir):
        path = root.split(os.sep)
        subdir = os.path.relpath(root, html_dir)
        for file in files:
            if '.html' in file:
                fix_circled_in_code_sections(os.path.join(html_dir, subdir, file))
