#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The conversion script outputs Markdown files in the same way they were
# strucutred in the LaTeX, i.e. in chapters. For mdBook to show the sections,
# we need to separated the content into section files.
# The script also creates the navigation tree file for mdBook.

import os
import sys
import re
import toml

# split files at section level for the nav tree to work
re_split_files = r'^(## (.*)[\s\S]+?)(?=\n## |\Z)'
# only chapter title and description
re_chapter_only = r'^(# [\s\S]+?)(?=\n## |\Z)'

###############################################################################

# splits files at section level, so mdbook lists sections in the navigation
# (files are expected to be split into chaters)
def split_files_at_sections(markdown_file):
    with open(markdown_file, 'r') as f:
        content = f.read()
        matches = re.findall(re_split_files, content, re.MULTILINE)
        
    sections = []
    for match in matches:
        section_name = match[1]
        section_content = match[0]
        
        section_filename = (re.sub(r'[\\/:"*?<>|]+', '', section_name)).replace(' ', '_').lower()
        chapter_dir = os.path.splitext(markdown_file)[0]
        chapter_name = os.path.splitext(os.path.basename(markdown_file))[0]
        os.makedirs(chapter_dir, exist_ok=True)
        section_file = os.path.join(chapter_dir,
                                    section_filename + '.md')
        with open(section_file, 'w') as f:
            f.seek(0)
            f.truncate()
            f.write(section_content)
        sections.append([section_name, '{0}/{1}.md'.format(chapter_name, section_filename)])
    return sections

# remove all content after the chapter description
def remove_sections_from_chapter(markdown_file):
    with open(markdown_file, 'r+') as f:
        content = f.read()
        content = re.search(re_chapter_only, content, re.MULTILINE).group()
        
        f.seek(0)
        f.truncate()
        f.write(content)
        
def get_chapter_title(markdown_file):
    with open(markdown_file, 'r') as f:
        content = f.read()
        chapter_title = re.search(r'^\# (.*)', content, re.MULTILINE).group(1)
        return chapter_title

# check whether there is text between the chapter title and the first section        
def is_chapter_empty(markdown_file):
    with open(markdown_file, 'r') as f:
        content = f.read()
        chapter_content = re.search(r'^\#.*([\s]*?)^## ', content, re.MULTILINE)
        return chapter_content != None

################################################################################
if __name__ == "__main__":
    # read config
    if len(sys.argv) != 2:
        print('You must provide a config.toml file!')
        sys.exit(-1)
    config_file = sys.argv[1]
    config = toml.load(config_file)

    sources = config.get('latex').get('chapter').get('files')
    target_dir = config.get('markdown').get('directory')
    nav_tree_file = config.get('markdown').get('mdbook').get('summary').get('file')
    prefix_chapters = config.get('markdown').get('mdbook').get('summary').get('prefix')
    suffix_chapters = config.get('markdown').get('mdbook').get('summary').get('suffix')
    
    # change file structure and create navigation tree
    nav_tree = ''
    for prefix_chapter in prefix_chapters:
        nav_tree += '[{0}]({1})\n'.format(prefix_chapter[0], prefix_chapter[1])
        
    for source in sources:
        chapter_file = os.path.join(target_dir, source + ".md")
        chapter_title = get_chapter_title(chapter_file)
        if not is_chapter_empty(chapter_file):
            nav_tree += '- [{0}](./{1}.md)\n'.format(chapter_title, source)
        else:
            nav_tree += '- [{0}]()\n'.format(chapter_title)
        
        sections = split_files_at_sections(chapter_file)
        for section in sections:
            nav_tree += '    - [{0}](./{1})\n'.format(section[0], section[1])
            
        remove_sections_from_chapter(chapter_file)
            
    nav_tree += '---\n'
    for suffix_chapter in suffix_chapters:
        nav_tree += '[{0}]({1})\n'.format(suffix_chapter[0], suffix_chapter[1])
        
    with open(os.path.join(target_dir, nav_tree_file), 'w') as f:
        f.seek(0)
        f.truncate()
        f.write(nav_tree)
