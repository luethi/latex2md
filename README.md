# LaTeX2md

This is a very limited LaTeX to Markdown coverter that I use because pandoc did not do a very good conversion.

## Prerequisites

You will need:
    - python 3

Preferably create a virtual environment and install the dependencies:
```bash
pip install -r requirements.txt
```

## Usage

### Config file

The scripts needs a config file that contain all the necessary paths and files.
An complete example is shown below:
```toml
[latex.chapter]
directory = "../chapters"
files = [
    "abstract", 
    "introduction", 
    "literature-review", 
    "programming",
    "discussion",
    "conclusions"
]

[latex.bibliography]
directory = "../database"
file = "bibliography"

[markdown]
directory = "./src"

[markdown.mdbook.summary]
file = "SUMMARY.md"
prefix = [
    ['Title Page', './title_page.md']
]
suffix = [
    ['Bibliography', './bibliography.md']
]

[markdown.mdbook.html]
directory = "./book"
```

### Conversion commands

There are several scripts that solve different issues of the conversion:
1. `convert.py`: converts LaTeX to Markdown files
1. `restructure_files.py`: extract sections from Markdown files and put them into seperate files
1. `fix_circled.py`: applies a fix to mdBook HTML files for my custom LaTeX command that encircles numbers
1. `bib_reader.py`: helper class that converts a LaTeX bibliography to a Markdown file, also provides citation methods

The config file needs to be passed when calling the scripts. For exaple:
```bash
python latex2md/convert.py latex2md.toml
python latex2md/restructure_files.py latex2md.toml
python latex2md/fix_circled.py latex2md.toml
```

### Supported LaTeX commands

#### Sectioning

```latex
\chapter{xy}
\section{xy}
\subsection{xy}
\subsubsection{xy}
```

#### Text styling

```latex
\emph{text}
\textbf{text}
\texttt{text}
\url{link}
```

#### Lists

- `enumerate`
- `itemize`
- `description`

#### Figures, Tables & Listings

##### Images

```latex
\begin{figure}[position] % ignored
    \centering % ignored
    \subfile{images/sched-coop.tex}
    \caption{Cooperative (non-preemptive) scheduling}
    \label{fig:coop} % ignored
\end{figure}

% or

\begin{figure}[H]
    \includegraphics[width=\linewidth]{zephyr-arch.eps}
    \caption[Zephyr System Architecture]{Zephyr System Architecture (v.2.3.0) \cite{linux:zephyr}}
    \label{fig:zephyr-arch}
\end{figure}

```

Command sequence must be:
1. `\includegraphics` or `\subfile`
1. `\caption`

**Image path must contain file extension.**

**Images must be added to markdown source folder manually. Subfile, PDF and EPS must be converted to SVG manually.**

##### Tables

**Not really supported!**

```latex
\input{tables/a_table.tex}
```

Handeled as image: SVG of table must be added to the Markdown source folder manually.

##### Listings

```latex
\begin{customlisting}
\begin{minted}[escapeinside=||]{c}
void some_function(void);
\end{minted}

\caption{Implementing an interface using Rust traits and generics}
\label{lst:trait} % ignored
\end{customlisting}
```

Only `minted` is supported for syntax highlighting.

##### References

```latex
\autoref{fig:xy}
\autoref{tab:xy}
\autoref{lst:xy}
```

Will be replaced by *the figure/table/listing*.

#### Syntax highlighting (minted package)

```latex
\mintinline{rust}{set_position()}

\begin{minted}[escapeinside=||]{c}
void some_function(void);
\end{minted}
```

`\mintinline` disregards the language.

Code in the `minted` environment must not be indented.


#### Citations

The script creates IEEE style citations, i.e. \[3, pp. 56\], with links to the entry in the bibliography.

```latex
\cite{author:book}
\cite[9]{author:book}
\cite[~56]{author:book}
```

The bibliography is parsed to Markdown and added to the source files automatically.

#### Comments

Will be removed.


